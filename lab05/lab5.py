import Jetson.GPIO as GPIO
import time
# change these as desired they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 11
SPIMISO = 9
SPIMOSI = 10
SPICS = 8
output_pin1 = 4
output_pin2 = 18
#photoresistor connected to adc #0
photo_ch = 0

#port init
def init():
    #GPIO.BOARD GPIO.BCM GPIO.CVM GPIO.TEGRA SOC
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(output_pin1, GPIO.OUT, initial=GPIO.HIGH)
    GPIO.setup(output_pin2, GPIO.OUT, initial=GPIO.HIGH)
    GPIO.setwarnings(False)
    #GPIO.cleanup() #clean up at the end of your script
    # set up the SPI interface pins
    GPIO.setup(SPIMOSI, GPIO.OUT)
    GPIO.setup(SPIMISO, GPIO.IN)
    GPIO.setup(SPICLK, GPIO.OUT)
    GPIO.setup(SPICS, GPIO.OUT)

#read SPI data from MCP3008(or MCP3204) chip,8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
    if ((adcnum > 7) or (adcnum < 0)):
        return -1

    GPIO.output(cspin, True)
    GPIO.output(clockpin, False) # start clock low #clockping
    GPIO.output(cspin, False) # bring CS low 
    commandout = adcnum
    commandout |= 0x18
    commandout <<= 3 
    for i in range(5):
        if (commandout & 0x80):# commandout 1 - 0x80(0b10000000)&commandout
            GPIO.output(mosipin, True)
        else:
            GPIO.output(mosipin, False)
        commandout <<= 1
        GPIO.output(clockpin, True) # clockpin(rising edge)
        GPIO.output(clockpin, False) # clockpin(falling edge)
    adcout = 0 
    # read in one bit, one null bit and 10 ADB bits.
    for i in range(12):
        GPIO.output(clockpin, True) # #clockpin(rising edge)
        GPIO.output(clockpin, False) # #clockpin(falling edge)
        adcout <<= 1 
        if (GPIO.input(misopin)): # MISO
            adcout |= 0x1 
    
    GPIO.output(cspin, True) #cspin 1
    
    # first bit is 'null' so drop it.
    adcout >>= 1
    return adcout


def main():
    init()
    while True:
	#GPIO.setmode(GPIO.BCM)
        adc_value=readadc(photo_ch, SPICLK, SPIMOSI, SPIMISO, SPICS)
        print(adc_value)
	#GPIO.setmode(GPIO.BOARD)
        if adc_value > 0:
            GPIO.output(output_pin1, GPIO.HIGH)  # set output pin to high if adc_value > 100
            print("port 396 on")
        else:
            GPIO.output(output_pin1, GPIO.LOW)  # set output pin to low otherwise
            print("port 396 off")
        if adc_value > 100:
            GPIO.output(output_pin2, GPIO.HIGH)  # set output pin to high if adc_value > 500
            print("port 297 on")
        else:
            GPIO.output(output_pin2, GPIO.LOW)  # set output pin to low otherwise
            print("port 297 off")
        time.sleep(1)

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print("An error occurred: {}".format(e))



#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "gpio_class.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void scoreChanged(int newScore);

    void stateChanged(int newState);

private slots:

    void on_led_shining_clicked();

    void updateLED_cont();

    void on_led_switching_on_clicked();

    void on_led_switching_off_clicked();

    void on_led_sw_clicked();

    void updateLED_limit();

    void on_qte_btn_clicked();

    void checkButtonPress(int gpioNumber);

    void handleTimeout();

    void on_qte_btn_466_clicked();

    void on_qte_btn_396_clicked();

    void on_qte_btn_392_clicked();

    void on_qte_btn_429_clicked();

    void changeScore(int newScore);

    void changeState(int newState);

    void updateTimerInterval(int value);

private:
    Ui::MainWindow *ui;
    GPIOController gpio466{466}, gpio396{396}, gpio392{392}, gpio429{429};
    std::vector<GPIOController*> gpios={&gpio466, &gpio396, &gpio392, &gpio429};
    QTimer *timer = nullptr;
    QTimer *ledTimer = nullptr;
    int x,y, flashCount, randomIndex;
    int score =0;
    int state=0;
    int z=1000;
};

#endif // MAINWINDOW_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "gpio_class.h"
#include <QTimer>
#include <QTime>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->port466_off->setVisible(1);
    ui->port429_off->setVisible(1);
    ui->port396_off->setVisible(1);
    ui->port392_off->setVisible(1);
    ui->port466_on->setVisible(0);
    ui->port396_on->setVisible(0);
    ui->port429_on->setVisible(0);
    ui->port392_on->setVisible(0);
    connect(ui->led_speed_bar, SIGNAL(valueChanged(int)), this, SLOT(updateTimerInterval(int)));
    connect(this, SIGNAL(scoreChanged(int)), ui->qte_score, SLOT(display(int)));
    connect(this, SIGNAL(stateChanged(int)), ui->qte_state, SLOT(display(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_led_shining_clicked()
{
    gpio466.setDirection("out");
    gpio396.setDirection("out");
    gpio392.setDirection("out");
    gpio429.setDirection("out");
    if(ui->port466->isChecked()) {
        gpio466.setValue(true);
        ui->port466_on->setVisible(1);
        ui->port466_off->setVisible(0);
    }
	else {
        gpio466.setValue(false);
        ui->port466_off->setVisible(1);
        ui->port466_on->setVisible(0);
    }
	if(ui->port396->isChecked()) {
        gpio396.setValue(true);
        ui->port396_on->setVisible(1);
        ui->port396_off->setVisible(0);
    }
	else {
        gpio396.setValue(false);
        ui->port396_off->setVisible(1);
        ui->port396_on->setVisible(0);
    }
	if(ui->port392->isChecked()) {
        gpio392.setValue(true);
        ui->port392_on->setVisible(1);
        ui->port392_off->setVisible(0);
    }
	else {
        gpio392.setValue(false);
        ui->port392_off->setVisible(1);
        ui->port392_on->setVisible(0);
    }
	if(ui->port429->isChecked()) {
        gpio429.setValue(true);
        ui->port429_on->setVisible(1);
        ui->port429_off->setVisible(0);
    }
	else {
        gpio429.setValue(false);
        ui->port429_off->setVisible(1);
        ui->port429_on->setVisible(0);
    }
}

void MainWindow::updateLED_cont()
{
    static bool toggle = true;

	if(toggle) {
        gpio466.setValue(true);
        gpio396.setValue(true);
        gpio392.setValue(false);
        gpio429.setValue(false);
		ui->port466_on->setVisible(1);
		ui->port466_off->setVisible(0);
		ui->port396_on->setVisible(1);
		ui->port396_off->setVisible(0);
		ui->port392_off->setVisible(1);
		ui->port392_on->setVisible(0);
		ui->port429_off->setVisible(1);
		ui->port429_on->setVisible(0);
	}
	else {
        gpio466.setValue(false);
        gpio396.setValue(false);
        gpio392.setValue(true);
        gpio429.setValue(true);
		ui->port466_off->setVisible(1);
		ui->port466_on->setVisible(0);
		ui->port396_off->setVisible(1);
		ui->port396_on->setVisible(0);
		ui->port392_on->setVisible(1);
		ui->port392_off->setVisible(0);
		ui->port429_on->setVisible(1);
		ui->port429_off->setVisible(0);
	}

	toggle = !toggle;

}


void MainWindow::on_led_switching_on_clicked()
{
    gpio466.setDirection("out");
    gpio396.setDirection("out");
    gpio392.setDirection("out");
    gpio429.setDirection("out");
    timer = nullptr;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateLED_cont()));
    timer->start(z);
}

void MainWindow::on_led_switching_off_clicked()
{
    gpio466.setValue(false);
    gpio396.setValue(false);
    gpio392.setValue(false);
    gpio429.setValue(false);
    ui->port466_off->setVisible(1);
    ui->port466_on->setVisible(0);
    ui->port396_off->setVisible(1);
    ui->port396_on->setVisible(0);
    ui->port392_off->setVisible(1);
    ui->port392_on->setVisible(0);
    ui->port429_off->setVisible(1);
    ui->port429_on->setVisible(0);
    timer->stop();
}

void MainWindow::on_led_sw_clicked()
{
    y = ui->led_sw_num->text().toInt();
    gpio466.setDirection("out");
    gpio396.setDirection("out");
    gpio392.setDirection("out");
    gpio429.setDirection("out");
    timer = nullptr;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateLED_limit()));
    timer->start(z);
}

void MainWindow::updateLED_limit()
{
    static int i = 0;
    static bool toggle = true;

    if(toggle) {
        gpio466.setValue(true);
        gpio396.setValue(true);
        gpio392.setValue(false);
        gpio429.setValue(false);
        ui->port466_on->setVisible(1);
        ui->port466_off->setVisible(0);
        ui->port396_on->setVisible(1);
        ui->port396_off->setVisible(0);
        ui->port392_off->setVisible(1);
        ui->port392_on->setVisible(0);
        ui->port429_off->setVisible(1);
        ui->port429_on->setVisible(0);
    }
    else {
        gpio466.setValue(false);
        gpio396.setValue(false);
        gpio392.setValue(true);
        gpio429.setValue(true);
        ui->port466_off->setVisible(1);
        ui->port466_on->setVisible(0);
        ui->port396_off->setVisible(1);
        ui->port396_on->setVisible(0);
        ui->port392_on->setVisible(1);
        ui->port392_off->setVisible(0);
        ui->port429_on->setVisible(1);
        ui->port429_off->setVisible(0);
    }

    toggle = !toggle;
    if(++i >= 2*y) {
            gpio392.setValue(false);
            gpio429.setValue(false);
            ui->port392_off->setVisible(1);
            ui->port392_on->setVisible(0);
            ui->port429_off->setVisible(1);
            ui->port429_on->setVisible(0);
            i = 0;
            toggle = true;
            timer->stop();
    }
}

void MainWindow::on_qte_btn_clicked()
{
    changeScore(0);
    changeState(1);
    ledTimer = new QTimer(this);
    flashCount = 0;
    qsrand(QTime::currentTime().msec());
    randomIndex = qrand() % 4;
    gpios[randomIndex]->setValue(true);
    if (randomIndex==0){
        ui->port466_on->setVisible(1);
        ui->port466_off->setVisible(0);
    }
    else if (randomIndex==1){
        ui->port396_on->setVisible(1);
        ui->port396_off->setVisible(0);
    }
    else if (randomIndex==2){
        ui->port392_on->setVisible(1);
        ui->port392_off->setVisible(0);
    }
    else if (randomIndex==3){
        ui->port429_on->setVisible(1);
        ui->port429_off->setVisible(0);
    }
    connect(ledTimer, SIGNAL(timeout()), this, SLOT(handleTimeout()));
    ledTimer->start(200);
}

void MainWindow::checkButtonPress(int gpioNumber) {
    if (state == 1 && gpioNumber == gpios[randomIndex]->getNumber()) {
        ledTimer->stop();
        flashCount = 0;
        changeScore(score+20);
        if (score >= 100) {
            changeState(2);// End the game if the score reaches 100
            ui->port466_off->setVisible(1);
            ui->port429_off->setVisible(1);
            ui->port396_off->setVisible(1);
            ui->port392_off->setVisible(1);
            ui->port466_on->setVisible(0);
            ui->port396_on->setVisible(0);
            ui->port429_on->setVisible(0);
            ui->port392_on->setVisible(0);
        }
        else {
            // If the game is not over, start the next round
            ui->port466_off->setVisible(1);
            ui->port429_off->setVisible(1);
            ui->port396_off->setVisible(1);
            ui->port392_off->setVisible(1);
            ui->port466_on->setVisible(0);
            ui->port396_on->setVisible(0);
            ui->port429_on->setVisible(0);
            ui->port392_on->setVisible(0);
            flashCount = 0;
            randomIndex = qrand() % 4;
            gpios[randomIndex]->setValue(true);
            if (randomIndex==0){
                ui->port466_on->setVisible(1);
                ui->port466_off->setVisible(0);
            }
            else if (randomIndex==1){
                ui->port396_on->setVisible(1);
                ui->port396_off->setVisible(0);
            }
            else if (randomIndex==2){
                ui->port392_on->setVisible(1);
                ui->port392_off->setVisible(0);
            }
            else if (randomIndex==3){
                ui->port429_on->setVisible(1);
                ui->port429_off->setVisible(0);
            }
            ledTimer->start(200);
        }
    }
    else {
        ledTimer->stop();
        changeScore(0);
        changeState(0);
        ui->port466_off->setVisible(1);
        ui->port429_off->setVisible(1);
        ui->port396_off->setVisible(1);
        ui->port392_off->setVisible(1);
        ui->port466_on->setVisible(0);
        ui->port396_on->setVisible(0);
        ui->port429_on->setVisible(0);
        ui->port392_on->setVisible(0);
    }
}

void MainWindow::handleTimeout() {
    bool currentValue = gpios[randomIndex]->getValue();
    gpios[randomIndex]->setValue(!currentValue);
    flashCount++;
    if(gpios[randomIndex]->getValue()==0){
        if (randomIndex==0){
            ui->port466_off->setVisible(1);
            ui->port466_on->setVisible(0);
        }
        else if (randomIndex==1){
            ui->port396_off->setVisible(1);
            ui->port396_on->setVisible(0);
        }
        else if (randomIndex==2){
            ui->port392_off->setVisible(1);
            ui->port392_on->setVisible(0);
        }
        else if (randomIndex==3){
            ui->port429_off->setVisible(1);
            ui->port429_on->setVisible(0);
        }
    }
    else if (gpios[randomIndex]->getValue()==1){
        if (randomIndex==0){
            ui->port466_on->setVisible(1);
            ui->port466_off->setVisible(0);
        }
        else if (randomIndex==1){
            ui->port396_on->setVisible(1);
            ui->port396_off->setVisible(0);
        }
        else if (randomIndex==2){
            ui->port392_on->setVisible(1);
            ui->port392_off->setVisible(0);
        }
        else if (randomIndex==3){
            ui->port429_on->setVisible(1);
            ui->port429_off->setVisible(0);
        }
    }
    if (flashCount >= 10) {
        ledTimer->stop();
        flashCount = 0;
        changeScore(0);
        changeState(0);
        ui->port466_off->setVisible(1);
        ui->port429_off->setVisible(1);
        ui->port396_off->setVisible(1);
        ui->port392_off->setVisible(1);
        ui->port466_on->setVisible(0);
        ui->port396_on->setVisible(0);
        ui->port429_on->setVisible(0);
        ui->port392_on->setVisible(0);
    }
}
void MainWindow::on_qte_btn_466_clicked()
{
    if (ledTimer->isActive()){
        checkButtonPress(466);
    }
}

void MainWindow::on_qte_btn_396_clicked()
{
    if (ledTimer->isActive()){
        checkButtonPress(396);
    }
}

void MainWindow::on_qte_btn_392_clicked()
{
    if (ledTimer->isActive()){
        checkButtonPress(392);
    }

}

void MainWindow::on_qte_btn_429_clicked()
{
    if (ledTimer->isActive()){
        checkButtonPress(429);
    }
}

void MainWindow::changeScore(int newScore){
    score = newScore;
    emit scoreChanged(newScore);
}

void MainWindow::changeState(int newState){
    state = newState;
    emit stateChanged(newState);
}

void MainWindow::updateTimerInterval(int value)
{
    z = 10000 / value;
    if (timer != nullptr) {
        timer->setInterval(z);
    }
}

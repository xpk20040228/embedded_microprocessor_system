#include "gpio_class.h"

GPIOController::GPIOController(int gpioNumber) {
    this->gpioNumber = gpioNumber;
    this->exportGPIO();
}

GPIOController::~GPIOController() {
    //this->unexportGPIO();
}

int GPIOController::exportGPIO() {
    std::string exportPath = GPIO_PATH "export";
    return writeToFile(exportPath, std::to_string(this->gpioNumber));
}

int GPIOController::unexportGPIO() {
    std::string unexportPath = GPIO_PATH "unexport";
    return writeToFile(unexportPath, std::to_string(this->gpioNumber));
}

int GPIOController::setDirection(std::string direction) {
    std::string setDirectionPath = GPIO_PATH "gpio" + std::to_string(this->gpioNumber) + "/direction";
    return writeToFile(setDirectionPath, direction);
}

int GPIOController::setValue(bool value) {
    std::string setValuePath = GPIO_PATH "gpio" + std::to_string(this->gpioNumber) + "/value";
    return writeToFile(setValuePath, value ? "1" : "0");
}

int GPIOController::writeToFile(std::string path, std::string value) {
    std::ofstream fileStream(path.c_str());
    if (!fileStream) {
        std::cerr << "Unable to open file: " << path << std::endl;
        return -1;
    }
    fileStream << value;
    fileStream.close();
    return 0;
}

int GPIOController::getNumber(){
    return gpioNumber;
}

bool GPIOController::getValue() {
    std::string getValuePath = GPIO_PATH "gpio" + std::to_string(this->gpioNumber) + "/value";
    std::ifstream fileStream(getValuePath.c_str());
    if (!fileStream) {
        std::cerr << "Unable to open file: " << getValuePath << std::endl;
        return false;
    }
    std::string value;
    fileStream >> value;
    fileStream.close();
    return value == "1";
}

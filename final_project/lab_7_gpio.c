#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

void gpio_set(int gpioNum, int status){
	char buf[10];
	FILE *fp = fopen("/dev/demo", "w");
	if(fp == NULL) 
	{
		printf("can't open device!\n");
		return;
	}
	sprintf(buf,"%d%d",gpioNum,status);
	buf[4]='\0';
	printf("buf:%s\n",buf);
	fwrite(buf, strlen(buf), 1, fp); //write
	fclose(fp);
}

void gpio_get(int gpioNum){
	char buf[10];
	char read[10];
	FILE *fp = fopen("/dev/demo", "r+");
	if(fp == NULL) 
	{
		printf("can't open device!\n");
		return;
	}
	sprintf(buf,"%d",gpioNum);
	buf[3]='\0';
	printf("buf:%s\n",buf);
	fwrite(buf, strlen(buf), 1, fp);
	fread(read, 3, 1, fp); //read 
	read[1]='\0';
	printf("gpio %d status: %s\n", gpioNum, read);
	fclose(fp);
}


int main(int argc, char** argv) {
	int gpioNum;
	if (argc<2 || argc>3){
		printf("error\n");
		return -1;
	}
	else if (argc == 2){
		printf("mode get\n");
		gpioNum = atoi(argv[1]);
		gpio_get(gpioNum);
	}
	else if (argc == 3){
		printf("mode set\n");
		int gpioStatus;
		gpioNum = atoi(argv[1]);
		if (strcmp(argv[2],"on")==0){
			gpioStatus=1;
		}
		else if (strcmp(argv[2],"off")==0){
			gpioStatus=0;
		}
		else {
			printf("error\n");
			return -1;
		}
		gpio_set(gpioNum,gpioStatus);
	}
	return 0;
}

// Import necessary modules
const express = require('express');
const path = require('path');
const app = express();
const { execFile } = require('child_process'); // Import child_process module
const { spawn } = require('child_process');
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
// Create a mapping from LED names to GPIO numbers
const ledToGpio = {
    'led1': 392,
    'led2': 396
};
// Handle GET request for root URL
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'));
});
// Handle form submissions for LED control
app.post('/led-control', (req, res) => {
	console.log('Received request for /led-control'); // Add this line
    // Extract LED states and the state ('on' or 'off') from the form data
    const ledStates = req.body;
    const state = req.body.state; // 'on' or 'off'
    // Control LEDs based on the form data
    for (let led in ledToGpio) {
        if (ledToGpio.hasOwnProperty(led)) {
            if (ledStates[led]) { // Check if the checkbox for this LED was selected
                let gpio_num = ledToGpio[led]; // Get the corresponding GPIO number
                let command = ['./gpio', gpio_num.toString(), state];

                execFile('sudo', command, (error, stdout, stderr) => {
                    if (error) {
                        console.log(`error: ${error.message}`);
                        return;
                    }
                    if (stderr) {
                        console.log(`stderr: ${stderr}`);
                        return;
                    }
                    console.log(`stdout: ${stdout}`);
                });
            }
        }
    }
    // Send a response back to the client
    res.send('LED states updated successfully!');
});

// Handle form submissions for Mode_Shine
app.post('/led-mode-shine', (req, res) => {
	console.log('Received request for /led-mode-shine'); // Add this line
    let mode = req.body.mode;
    let command = ['./gpio', 'Mode_Shine', mode];
    execFile('sudo', command, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
    });
    res.send('LED mode updated successfully!');
});

let pythonProcess = null; // Keep a reference to the Python process
let adcValue ='';
app.post('/detect-control', (req, res) => {
    console.log('Received request for /detect-control');
    const detectionState = req.body.detect; // 'detect' or 'no_detect'
	let cur_adcValue='';
    if (detectionState === 'detect') {
        if (!pythonProcess) {
            // Execute the Python script
            const pythonScriptPath = 'adc_script.py'; // Replace with the actual path to your Python script
            pythonProcess = spawn('python3', [pythonScriptPath]);
            pythonProcess.stdout.on('data', (data) => {
                cur_adcValue = data.toString();
                console.log(`ADC Value: ${cur_adcValue}`);
				adcValue=cur_adcValue
            });
            pythonProcess.stderr.on('data', (data) => {
                console.error(`Python script stderr: ${data}`);
            });
            pythonProcess.on('close', (code) => {
                console.log(`Python script exited with code ${code}`);
            });
        }
		else {
            console.log('Python process already running.');
			res.json({
                    adcValue: adcValue,
                    showAdc: true,
                    showLedControl: false,
                    showLedModeShine: false
            });
        }
    }
	else if (detectionState === 'no_detect') {
        // Stop the Python script
        if (pythonProcess) {
            try {
                pythonProcess.kill();
            }
			catch (err) {
                console.error(`Failed to kill Python process: ${err}`);
            }
            pythonProcess = null;
        }
        // Send a JSON response to hide the ADC value on the webpage and to show detectControlForm, ledControlForm
        res.json({
            showAdc: false,
            showLedControl: true,
            showLedModeShine: false
        });
    }
});


// Start the server
app.listen(3000, () => console.log('Server is running on port 3000'));

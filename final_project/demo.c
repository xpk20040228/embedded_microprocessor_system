#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <asm/segment.h>
#include <linux/kthread.h>
#include <linux/uaccess.h>

#define MAJOR_NUM 60
#define MODULE_NAME "demo"
#define GPIO_PATH "/sys/class/gpio/"

static int iCount = 0;
static char userChar[100];
char buff_gpio[10];
char buff_status[5];
char fileName[50];
char read[10];
ssize_t ret;


static ssize_t drv_read(struct file *filp, char *buf, size_t count, loff_t *ppos);
static ssize_t drv_write(struct file *filp, const char *buf, size_t count, loff_t *ppos);
static long drv_ioctl (struct file *file, unsigned int ioctl_num, unsigned long ioctl_param);
static int drv_open(struct inode *inode, struct file *file);
static int drv_release(struct inode *inode, struct file *file);


struct file_operations drv_fops =
{
	.read = drv_read,
	.write = drv_write,
	.unlocked_ioctl = drv_ioctl,
	.open = drv_open,
	.release = drv_release,
};

static int demo_init(void)
{
	if (register_chrdev (MAJOR_NUM, "demo", &drv_fops) < 0)
	{
		printk("<1>%s: can't get major %d\n", MODULE_NAME, MAJOR_NUM); 
		return (-EBUSY);
	}
	printk("<1>%s: started\n", MODULE_NAME);
	return 0;
}

static void demo_exit(void)
{	
	unregister_chrdev (MAJOR_NUM, "demo");
	printk("<1>%s: removed\n", MODULE_NAME);
}


static ssize_t drv_read(struct file *filp, char *buf, size_t count, loff_t *ppos)
{
	printk("Read: Enter Read function\n");
	printk("device read\n");
	printk("readChar: %s\n", read);
	printk("R_buf_size: %ld\n", strlen(read));
	
	if (copy_to_user(buf, read, count)) {
        return -EFAULT;
    }
	
	iCount++;
	return count;
}


static int thread_direction(void *data) {
	char read_temp[10];
	char fileName1[50];
	loff_t pos =0;
	struct file *fp2;
	struct file *fp4;
	mm_segment_t old_fs2;
	mm_segment_t old_fs4;
	
	sprintf(fileName1, "%s%s%s%s",GPIO_PATH, "gpio", buff_gpio,"/direction");
	fp2 = filp_open(fileName1, O_WRONLY, 0);
	if (IS_ERR(fp2)) {
		printk(KERN_ERR "Failed to open %s: %ld\n", fileName1, PTR_ERR(fp2));
		return PTR_ERR(fp2);
	}
	old_fs2 = get_fs(); 
	set_fs(KERNEL_DS);
	printk("gpio file is set direction! %s\n",fileName1);
	ret=vfs_write(fp2, "out", 3, &pos);
	if (ret < 0) {
		printk("vfs_write failed with error code %zd\n", ret);
	}
	else {
		printk("vfs_write Successfully wrote %zd bytes\n", ret);
	}
	filp_close(fp2, NULL);
	set_fs(old_fs2);
	
	pos=0;
	memset(read_temp, 0, sizeof(read_temp));
	sprintf(fileName1, "%s%s%s%s",GPIO_PATH, "gpio", buff_gpio,"/direction");
	fp4 = filp_open(fileName1, O_RDONLY, 0);
	if (IS_ERR(fp4)) {
		printk(KERN_ERR "Failed to open %s: %ld\n", fileName1, PTR_ERR(fp4));
		return PTR_ERR(fp4);
	}
	old_fs4 = get_fs(); 
	set_fs(KERNEL_DS);
	printk("gpio file is read direction! %s\n",fileName1);
	ret=vfs_read(fp4, read_temp, 4, &pos);
	if (ret < 0) {
		printk("vfs_read failed with error code %zd\n", ret);
	}
	else {
		printk("vfs_read Successfully read %zd bytes\n", ret);
	}
	read_temp[3]='\0';
	printk("gpioPath: %s; gpioDirection:%s\n",fileName1, read_temp);
	filp_close(fp4, NULL);
	set_fs(old_fs4);
    return 0;
}

static int thread_value(void *data) {
    char read_temp[10];
	char fileName2[50];
	loff_t pos =0;
	struct file *fp3;
	struct file *fp5;
	mm_segment_t old_fs3;
	mm_segment_t old_fs5;
	
	pos=0;
	sprintf(fileName2, "%s%s%s%s", GPIO_PATH, "gpio", buff_gpio,"/value");
	fp3 = filp_open(fileName2, O_WRONLY, 0);
	if (IS_ERR(fp3)) {
		printk(KERN_ERR "Failed to open %s: %ld\n", fileName2, PTR_ERR(fp3));
		return PTR_ERR(fp3);
	}
	old_fs3 = get_fs(); 
	set_fs(KERNEL_DS);
	printk("gpio file is set value! %s %s\n",fileName2,buff_status);
	ret=vfs_write(fp3, buff_status, 1, &pos);
	if (ret < 0) {
		printk("vfs_write failed with error code %zd\n", ret);
	}
	else {
		printk("vfs_write Successfully wrote %zd bytes\n", ret);
	}
	filp_close(fp3, NULL);
	set_fs(old_fs3);
	
	pos=0;
	memset(read_temp, 0, sizeof(read_temp));
	sprintf(fileName2, "%s%s%s%s", GPIO_PATH, "gpio", buff_gpio,"/value");
	fp5 = filp_open(fileName2, O_RDONLY, 0);
	if (IS_ERR(fp5)) {
		printk(KERN_ERR "Failed to open %s: %ld\n", fileName2, PTR_ERR(fp5));
		return PTR_ERR(fp5);
	}
	old_fs5 = get_fs(); 
	set_fs(KERNEL_DS);
	printk("gpio file is read value! %s\n",fileName2);
	ret=vfs_read(fp5, read_temp, 2, &pos);
	if (ret < 0) {
		printk("vfs_read failed with error code %zd\n", ret);
	}
	else {
		printk("vfs_read Successfully read %zd bytes\n", ret);
	}
	read_temp[1]='\0';
	printk("gpioPath: %s; gpioValue:%s\n",fileName2, read_temp);
	filp_close(fp5, NULL);
	set_fs(old_fs5);
	
    return 0;
}

static ssize_t drv_write(struct file *filp, const char *buf, size_t count, loff_t *ppos)
{
	printk("Write: Enter Write function\n");
	printk("device write\n");
	printk("%d\n", iCount);
	printk("W_buf_size: %d\n", (int)count);
	copy_from_user(userChar, buf, count);
	userChar[count] = '\0';
	printk("writeChar: %s\n", userChar);
	
	if ((int)count==4){
		printk("mode set\n");
	
		strncpy(buff_gpio, userChar, 3);
		buff_gpio[3] = '\0';
		buff_status[0]=userChar[3];
		buff_status[1] = '\0';
		printk("gpioNum:%s %ld gpioStat:%s %ld\n",buff_gpio,strlen(buff_gpio),buff_status,strlen(buff_status));
		
		struct file *fp1;
		mm_segment_t old_fs1;

		loff_t pos =0;
		sprintf(fileName, "%s%s", GPIO_PATH, "export");
		fp1 = filp_open(fileName, O_WRONLY, 0);
		if (IS_ERR(fp1)) {
			printk(KERN_ERR "Failed to open %s: %ld\n", fileName, PTR_ERR(fp1));
			return PTR_ERR(fp1);
		}
		old_fs1 = get_fs(); 
		set_fs(KERNEL_DS);
		printk("gpio file is exporting! %s %s %ld %lld\n",fileName,buff_gpio,strlen(buff_gpio),pos);
		ret=vfs_write(fp1, buff_gpio, strlen(buff_gpio), &pos);
		if (ret < 0) {
			printk("vfs_write failed with error code %zd\n", ret);
		}
		else {
			printk("vfs_write Successfully wrote %zd bytes\n", ret);
		}
		filp_close(fp1, NULL);
		set_fs(old_fs1);
		
		// Create and run the first pthread
		struct task_struct *thread1;
		thread1 = kthread_run(thread_direction, NULL, "thread_direction");
		if (IS_ERR(thread1)) {
			printk(KERN_ERR "Failed to create thread_direction\n");
			return PTR_ERR(thread1);
		}

		// Create and run the second pthread
		struct task_struct *thread2;
		thread2 = kthread_run(thread_value, NULL, "thread_value");
		if (IS_ERR(thread2)) {
			printk(KERN_ERR "Failed to create thread_value\n");
			return PTR_ERR(thread2);
		}

	}
	else if ((int)count==3){
		printk("mode get\n");
		strncpy(buff_gpio, userChar, 3);
		buff_gpio[3] = '\0';
		buff_status[0]='\0';
		printk("gpioNum:%s %ld\n",buff_gpio,strlen(buff_gpio));
		
		struct file *fp1;
		struct file *fp2;
		loff_t pos =0;
		mm_segment_t old_fs1;
		mm_segment_t old_fs2;
		
		sprintf(fileName, "%s%s", GPIO_PATH, "export");
		fp1 = filp_open(fileName, O_WRONLY, 0);
		if (IS_ERR(fp1)) {
			printk(KERN_ERR "Failed to open %s: %ld\n", fileName, PTR_ERR(fp1));
			return PTR_ERR(fp1);
		}
		old_fs1 = get_fs(); 
		set_fs(KERNEL_DS);
		printk("gpio file is exporting! %s %s %ld %lld\n",fileName,buff_gpio,strlen(buff_gpio),pos);
		ret=vfs_write(fp1, buff_gpio, strlen(buff_gpio), &pos);
		if (ret < 0) {
			printk("vfs_write failed with error code %zd\n", ret);
		}
		else {
			printk("vfs_write Successfully wrote %zd bytes\n", ret);
		}
		filp_close(fp1, NULL);
		set_fs(old_fs1);
		
		pos=0;
		memset(read, 0, sizeof(read));
		sprintf(fileName, "%s%s%s%s", GPIO_PATH, "gpio", buff_gpio,"/value");
		fp2 = filp_open(fileName, O_RDONLY, 0);
		if (IS_ERR(fp2)) {
			printk(KERN_ERR "Failed to open %s: %ld\n", fileName, PTR_ERR(fp2));
			return PTR_ERR(fp2);
		}
		old_fs2 = get_fs(); 
		set_fs(KERNEL_DS);
		printk("gpio file is read value! %s\n",fileName);
		ret=vfs_read(fp2, read, 2, &pos);
		if (ret < 0) {
			printk("vfs_read failed with error code %zd\n", ret);
		}
		else {
			printk("vfs_read Successfully read %zd bytes\n", ret);
		}
		read[1]='\0';
		printk("gpioPath: %s; GPIO %s status: %s\n",fileName,buff_gpio, read);
		filp_close(fp2, NULL);
		set_fs(old_fs2);
	}
	iCount++;
	return count;
}

static long drv_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param) {
	printk("I/O Control: Enter I/O Control function\n");
	printk("device ioctl\n");
	
	return 0;
}


static int drv_open(struct inode *inode, struct file *file) {
	printk("Open: Enter Open function\n");
	printk("device open\n");
	
	return 0;
}

static int drv_release(struct inode *inode, struct file *file) {
	printk("Release: Enter Release function\n");
	printk("device close\n");
	
	return 0;
}

module_init(demo_init);
module_exit(demo_exit);
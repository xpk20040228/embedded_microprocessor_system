#include "gpio_class.hpp"
#include <string>

void Mode_Shine(int x);

int main(int argc, char **argv) {
    if(argc != 3) {
        std::cerr << "Usage: gpio <portnum> <on/off> or gpio Mode_Shine <x>" << std::endl;
        return -1;
    }

    if(std::string(argv[1]) == "Mode_Shine") {
        Mode_Shine(std::stoi(argv[2]));
    }
	else {
        // Create a GPIOController for the specified GPIO number
        int gpioNumber = std::stoi(argv[1]);
        GPIOController gpio(gpioNumber);

        // Set the direction of the GPIO to out
        gpio.setDirection("out");

        // Set the value of the GPIO based on the second argument
        std::string state = argv[2];
        gpio.setValue(state == "on");
    }

    return 0;
}

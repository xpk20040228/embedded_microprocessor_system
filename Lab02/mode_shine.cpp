#include "gpio_class.hpp"
#include <thread>
#include <chrono>

void Mode_Shine(int x) {
    GPIOController gpio1(466), gpio2(396), gpio3(429), gpio4(392);
    gpio1.setDirection("out");
    gpio2.setDirection("out");
    gpio3.setDirection("out");
    gpio4.setDirection("out");

    for(int i = 0; i < x; i++) {
        gpio1.setValue(true);
        gpio2.setValue(true);
        gpio3.setValue(false);
        gpio4.setValue(false);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        gpio1.setValue(false);
        gpio2.setValue(false);
        gpio3.setValue(true);
        gpio4.setValue(true);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    gpio3.setValue(false);
    gpio4.setValue(false);
}

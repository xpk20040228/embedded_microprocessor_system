// Import necessary modules
const express = require('express');
const path = require('path');
const app = express();
const { execFile } = require('child_process'); // Import child_process module
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
// Create a mapping from LED names to GPIO numbers
const ledToGpio = {
    'led1': 466,
    'led2': 396,
    'led3': 392,
    'led4': 393
};
// Handle GET request for root URL
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'));
});
// Handle form submissions for LED control
app.post('/led-control', (req, res) => {
    // Extract LED states and the state ('on' or 'off') from the form data
    const ledStates = req.body;
    const state = req.body.state; // 'on' or 'off'
    // Control LEDs based on the form data
    for (let led in ledToGpio) {
        if (ledToGpio.hasOwnProperty(led)) {
            if (ledStates[led]) { // Check if the checkbox for this LED was selected
                let gpio_num = ledToGpio[led]; // Get the corresponding GPIO number
                let command = ['./gpio', gpio_num.toString(), state];

                execFile('sudo', command, (error, stdout, stderr) => {
                    if (error) {
                        console.log(`error: ${error.message}`);
                        return;
                    }
                    if (stderr) {
                        console.log(`stderr: ${stderr}`);
                        return;
                    }
                    console.log(`stdout: ${stdout}`);
                });
            }
        }
    }
    // Send a response back to the client
    res.send('LED states updated successfully!');
});
// Handle form submissions for Mode_Shine
app.post('/led-mode-shine', (req, res) => {
    let mode = req.body.mode;
    let command = ['./gpio', 'Mode_Shine', mode];
    execFile('sudo', command, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
    });
    res.send('LED mode updated successfully!');
});
// Start the server
app.listen(3000, () => console.log('Server is running on port 3000'));

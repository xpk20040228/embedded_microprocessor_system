#include "gpio_class.hpp"
#include <thread>
#include <chrono>

void Mode_Shine(int x) {
    GPIOController gpio1(392), gpio2(396);
    gpio1.setDirection("out");
    gpio2.setDirection("out");
    for(int i = 0; i < x; i++) {
        gpio1.setValue(true);
        gpio2.setValue(false);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        gpio1.setValue(false);
        gpio2.setValue(true);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    gpio2.setValue(false);
}

#ifndef GPIO_CLASS_H
#define GPIO_CLASS_H

#include <iostream>
#include <fstream>
#include <string>

// Define the path to the GPIO interface
#define GPIO_PATH "/sys/class/gpio/"

class GPIOController {
public:
    GPIOController(int gpioNumber);
    ~GPIOController();

    int exportGPIO();
    int unexportGPIO();
    int setDirection(std::string direction);
    int setValue(bool value);

private:
    int gpioNumber;
    int writeToFile(std::string path, std::string value);
};

#endif
